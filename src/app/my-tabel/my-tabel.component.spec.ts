import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyTabelComponent } from './my-tabel.component';

describe('MyTabelComponent', () => {
  let component: MyTabelComponent;
  let fixture: ComponentFixture<MyTabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyTabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyTabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
