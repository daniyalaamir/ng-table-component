import { Component, OnInit } from '@angular/core';
import { MyDataService } from '../my-data.service';

@Component({
  selector: 'app-my-tabel',
  templateUrl: './my-tabel.component.html',
  styleUrls: ['./my-tabel.component.scss']
})
export class MyTabelComponent implements OnInit {
  allData: any[] = [];
  pagedData: any[] = [];
  pageSize:Number = 50;
  totalRecords: Number = 100;
  currentPage: Number=1;

  pageSizeOptions:Number[] = [10, 50,100,250,500,1000];

  pager:Number[] = [1,2,3,4,5,6,7,8,9,10];

  cols: string[]= [];
  constructor(private dataService: MyDataService ) { }

  ngOnInit() {
    this.allData = this.dataService.getMyData();
    this.fillCol(this.allData[0]);
    this.totalRecords = this.allData.length;

    this.pageSize = 10;
    this.currentPage = 1;
    this.setPagedData();
    this.setPager();
  }
  setPagedData = function(){
    var stIdx = (this.currentPage * this.pageSize) - this.pageSize;
    var endIdx = this.currentPage * this.pageSize;
    this.pagedData = this.allData.slice(stIdx, endIdx);
  }
  setPager = function(){
    var stIdx = 1;
    var endIdx = Math.round(this.totalRecords/ this.pageSize);
    this.pager = [];
    
    for (let index = 1; index <= endIdx; index++) {
      this.pager.push(index);
    }
    
    if(this.totalRecords > this.pageSize* endIdx)
    {
      this.pager.push( endIdx + 1);
    }
  }
  pageSizeChanged = function($event){
    this.pageSize = $event.target.value;
    this.currentPage = 1;
    this.setPagedData();
    this.setPager();
  }
  pageChanged = function(pageNo){
    this.currentPage = pageNo;
    this.setPagedData();
  }
  private fillCol = function(item){
    this.cols = Object.keys(item);
  }
}
